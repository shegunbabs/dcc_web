<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('api_token', 60);
            $table->tinyInteger('verified')->default('0');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        \App\User::create([
            'username' => 'shegunbabs',
            'email' => 'shegun.babs@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
