<?php
/**
 * Created by PhpStorm.
 * User: segun
 * Date: 4/6/2018
 * Time: 6:30 PM
 */

namespace App\Observers;


use App\User;

class UserObserver
{

    public function creating(User $user)
    {
        $user->api_token = str_random(60);
        $user->verified = 0;
    }
}