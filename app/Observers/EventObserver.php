<?php
/**
 * Created by PhpStorm.
 * User: segun
 * Date: 5/25/2018
 * Time: 12:35 PM
 */

namespace App\Observers;


use App\Models\Event;

class EventObserver
{

    public function creating(Event $event)
    {
        $event->name_slug = str_slug($event->name);
    }
}