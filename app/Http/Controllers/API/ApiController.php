<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\EventRequest;
use App\Models\Event;
use App\Models\EventType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    public function newEventType(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:3|unique:event_types',
            'description' => 'required|min:3',
            'visibility' => 'required|in:0,1'
        ]);

        //save model
        EventType::create([
            'name' => lowerCase($data['name']),
            'description' => lowerCase($data['description']),
            'visibility' => $data['visibility'],
        ]);

        return ['success'];
    }


    public function allEventType()
    {
        return EventType::select('id', 'name', 'description', 'visibility')->get();
    }


    public function postEvent(EventRequest $request)
    {
        $request->save();
        return ['success'];
    }


    public function allEvents()
    {
        $data = Event::with('eventType')->paginate(7);
        return $data;
    }

    public function delEvent($id)
    {
        $data = Event::find($id);
        if ($data)
            $data->delete();
        return ['success'];
    }
}
