<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventController extends Controller
{


    /**
     * EventController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('backend.events.index');
    }


    public function create()
    {
        return view('backend.events.create');
    }
}
