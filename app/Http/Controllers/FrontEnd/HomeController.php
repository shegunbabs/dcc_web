<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index()
    {
        return view('frontend.index');
        dd('home index');
    }


    public function theChurch()
    {
        return view('frontend.pages.thechurch');
    }

    public function sermons()
    {
        return view('frontend.pages.sermons');
    }

    public function workforce(){}
}
