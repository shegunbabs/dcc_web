<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('backend.auth.login');
    }


    public function authenticated(Request $request, $user)
    {
        if ( $request->ajax() ){
            return response()->json([
                'auth' => auth()->check(),
                'user' => $user,
                'intended' => $this->fetchIntended(),
            ]);
        }
    }


    public function fetchIntended()
    {
        $path = session()->pull('url.intended', $this->redirectPath());
        return $path;
    }


    public function username()
    {
        return 'email';
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        flash()->overlay('test it');
        return redirect('/backend/');
    }


    private function redirectTo()
    {
        return route('backend.dashboard');
    }
}
