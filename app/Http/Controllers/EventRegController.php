<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventRegController extends Controller
{

    public function register($slug, Request $request)
    {
        $model = Event::where('name_slug', $slug)->first();
        return view('backend.events.single-reg-page', compact('model'));
    }
}
