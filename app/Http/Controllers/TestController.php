<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Http\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class TestController extends Controller
{
    public function monitor()
    {
        $servers = [
            [
                'name' => 'DCC Lagos I',
                'url' => 'dcclagos.tk'
            ],
            'DCC Lagos II' => 'dcclagos.tk',
        ];

        //$process = new Process('ping -c 4 '.$servers[0]['url']);
        $process = new Process('service --status-all');
        $process->run();

        if (!$process->isSuccessful()){
            echo 'Server unreachable';
        }

        $output = $process->getOutput();
        dd($output);

        preg_match_all("/\[\s+(\+?\-?)\s+\]?\s+([0-9a-zA-Z\-\.]+)/", $output, $matches);

        $watched_processes = ['redis-server', 'rabbitmq-server'];

        foreach( $watched_processes as $pros ){
            $key = array_search($pros, $matches[2]);

            if ($key !== FALSE){
                //echo "[" . "(String) $matches[1][$key]" . "]" . '=>' . (String) $matches[2][$key]. "<br/>";
                echo $matches[1][$key] . $matches[2][$key] . "<br>";
                //dd($matches[1][$key], $matches[2][$key]);
            }
        }
    }

    public function index(Request $request)
    {
        $id = 15;
        return encrypt($id);
        $d = Event::find($id);
        dd($d);

        $faker = Factory::create();
        $n = 30;
        for($i=0; $i<=$n; $i++)
        {
            Event::create([
                'name' => $faker->realText(35, 5),
                'theme' => $faker->realText(20, 1),
                'event_type_id' => $faker->randomElement([1, 2, 3]),
                'venue' => $faker->randomElement(['The DOM, main auditorium, behind Fatgbems', 'The church office, apple junction']),
                'reg_status' => $faker->randomElement([0, 1]),
                'start_date' => Carbon::now()->addDays(10),
                'end_date' => Carbon::now()->addDays(10),
            ]);
        }

        return;


        $pattern = "/[A-Za-z\'\"\_\-\*\|\;\:\,]+(?:-\w +)*/";
        $subject = "AWOL Men's Conference";
        dd(capitalize($subject));


        //preg_match_all($pattern, $subject, $match);
        $match = explode(" ", $subject);

        $out = [];
        //$match = $match[0];
        foreach($match as $word){

            $firstTwoLetters = substr($word, 0, 2);

            if ($firstTwoLetters === strtoupper($firstTwoLetters))
                $out[] = $word;
            else
                $out[] = strtolower($word);
        }
        dd(implode(" ", $out));

        return;
    }
}
