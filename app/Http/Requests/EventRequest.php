<?php

namespace App\Http\Requests;

use App\Models\Event;
use App\Models\EventType;
use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'theme'         => 'required',
            'event_type_id' => 'required',
            'venue'         => 'required',
            'reg_status'    => 'required',
            'start_date'    => 'required',
            'end_date'      => 'required',
        ];
    }


    public function save()
    {
        $this->dbSave();
    }


    private function dbSave()
    {
        EventType::find($this->event_type_id)->events()->
        //Event::
        create([
            'name' => lowerCase($this->name),
            'theme' => lowerCase($this->theme),
            //'event_type_id' => $this->event_type_id,
            'venue' => lowerCase($this->venue),
            'reg_status' => $this->reg_status,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
        ]);
    }
}
