<?php

use Illuminate\Http\Request;


function capitalize($sentence)
{
    $match = explode(" ", $sentence);
    $out = [];
    foreach ($match as $word)
    {
        $firstTwoLetters = substr($word, 0, 2);
        if ($firstTwoLetters === strtoupper($firstTwoLetters)){
            $out[] = $word;
        }else{
            $out[] = ucfirst($word);
        }
    }
    return implode(" ", $out);
}


function lowerCase($string)
{
    $match = explode(" ", $string);
    $out = [];
    foreach ($match as $word)
    {
        $firstTwoLetters = substr($word, 0, 2);
        if ($firstTwoLetters === strtoupper($firstTwoLetters)){
            $out[] = $word;
        }else{
            $out[] = strtolower($word);
        }
    }
    return implode(" ", $out);
}


function markActiveLink($url)
{
    $requestUri = app('request')->getUri();
    echo $url == $requestUri ? ' active' : '';
}


function markActiveParentLink($url)
{
    $requestUri = app('request')->getUri();
    echo before_last("/", $requestUri) == before_last("/", $url) ? ' active':'';
}


function strrevpos($instr, $needle)
{
    $rev_pos = strpos (strrev($instr), strrev($needle));
    if ($rev_pos===false) return false;
    else return strlen($instr) - $rev_pos - strlen($needle);
};
//after ('@', 'biohazard@online.ge');
//returns 'online.ge'
//from the first occurrence of '@'
function after($_this, $inthat)
{
    if (!is_bool(strpos($inthat, $_this)))
        return substr($inthat, strpos($inthat, $_this) + strlen($_this));
}


//after_last ('[', 'sin[90]*cos[180]');
//returns '180]'
//from the last occurrence of '['
function after_last($_this, $inthat)
{
    if (!is_bool(strrevpos($inthat, $_this)))
        return substr($inthat, strrevpos($inthat, $_this) + strlen($_this));
}


//before ('@', 'biohazard@online.ge');
//returns 'biohazard'
//from the first occurrence of '@'
function before($_this, $inthat)
{
    return substr($inthat, 0, strpos($inthat, $_this));
}


//before_last ('[', 'sin[90]*cos[180]');
//returns 'sin[90]*cos['
//from the last occurrence of '['
function before_last($_this, $inthat)
{
    return substr($inthat, 0, strrevpos($inthat, $_this));
}


//between ('@', '.', 'biohazard@online.ge');
//returns 'online'
//from the first occurrence of '@'
/**
 * @param $_this
 * @param $that
 * @param $inthat
 * @return string
 */
function between($_this, $that, $inthat)
{
    return before($that, after($_this, $inthat));
}


//between_last ('[', ']', 'sin[90]*cos[180]');
//returns '180'
//from the last occurrence of '['
function between_last($_this, $that, $inthat)
{
    return after_last($_this, before_last($that, $inthat));
}

//Take the no. of characters length
//from a string and indicate how many to skip before taking
function str_take($length, $string, $skip=0)
{
    return substr($string, $skip, $length);
}

//Take from right to left the no. of characters length
//from a string and indicate how many to skip before taking
function str_rtake($length, $string, $skip=0)
{
    if ($skip == 0){
        return substr($string, -$length);
    } elseif ($skip > 0){
        $skip = $skip  + $length;
        return substr($string, -$skip, $length);
    }
}