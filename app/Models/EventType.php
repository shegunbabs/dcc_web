<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{

    protected $fillable = ['name', 'description', 'visibility'];


    public function events()
    {
        return $this->hasMany(Event::class);
    }


    public function getNameAttribute($value)
    {
        return capitalize($value);
    }

    public function getDescriptionAttribute($value)
    {
        return capitalize($value);
    }
}
