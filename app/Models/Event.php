<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    protected $fillable = ['name', 'theme', 'event_type_id', 'venue', 'reg_status', 'start_date', 'end_date'];


    public function eventType()
    {
        return $this->belongsTo(EventType::class);
    }


    public function getNameAttribute($value)
    {
        return capitalize($value);
    }


    public function getThemeAttribute($value)
    {
        return capitalize($value);
    }

    public function getVenueAttribute($value)
    {
        return capitalize($value);
    }
}
