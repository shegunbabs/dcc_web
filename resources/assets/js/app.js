
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Noty = require('noty');
window.Swal = require('sweetalert');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('date-picker', require('./components/DatePickerComponent.vue'));
Vue.component('app-paginator', require('./components/AppPaginator.vue'));
Vue.component('modal-dialog', require('./components/RegLinkComponent.vue'));
Vue.component('widget-card', require('./components/DashboardWidget.vue'));

// const app = new Vue({
//     el: '#app'
// });


document.addEventListener("DOMContentLoaded", (e) => {

    let winHeight = window.innerHeight;

    if (document.getElementsByTagName('html')[0]){
        document.getElementsByTagName('html')[0].classList.remove('no-js')
    }
    document.getElementById('loader').classList.remove("active");

    $('#flash-overlay-modal').modal();
    $('div.alert').not('.alert-important').delay(3000).fadeOut(550);

    var Spark = {
        animateWidgetsAfterPageLoad: function(){
            $(document).ready(function() {
                var speed = 2000;
                var delay = 0;
                var container = $('.page-content');
                container.each(function() {
                    var elements = $(this).find('.widget');
                    elements.each(function() {
                        var elementOffset = $(this).offset();
                        var offset = elementOffset.left * 0.8 + elementOffset.top;
                        var delay = (parseFloat(offset / speed) - 0.3).toFixed(2);
                        $(this)
                            .css('-webkit-animation-delay', delay + 's')
                            .css('-o-animation-delay', delay + 's')
                            .css('animation-delay', delay + 's');
                        $(this).addClass('animated');
                    });
                });
            });
        },

        createSidebar: function(){
            $('.sidebar-collapse').on('click', function(){
                $('.page-body').toggleClass('collapsed');
            });
            $('.sidebar-open').on('click', function(){
                $('.page-sidebar').removeClass('toggled');
            });
            $('.sidebar-close').on('click', function(){
                $('.page-sidebar').addClass('toggled');
            });

            $('.nav-stacked').on('show.bs.collapse', function () {
                $('.nav-stacked .in').collapse('hide');
            });
        },

        createWidgets: function() {

            $('.widget-minify').on('click', function(e){
                e.preventDefault();
                $(this).closest('.widget').toggleClass('collapsed');
                $(this).find('i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
            });

            // $('.widget-close').on('click', function(e){
            //   e.preventDefault();
            //   $(this).closest('.widget').hide();
            // });
        },

        callOnResize: [],
        handleElementsOnResizing: function() {
            var resizing;
            $(window).resize(function() {
                if (resizing) {
                    clearTimeout(resizing);
                }
                resizing = setTimeout(function() {
                    for (var i = 0; i < Spark.callOnResize.length; i++) {
                        Spark.callOnResize[i].call();
                    }
                }, 300);
            });
        },

        init: function(){
            this.animateWidgetsAfterPageLoad();
            this.createSidebar();
            this.createWidgets();
            this.handleElementsOnResizing();
        }
    };
    Spark.init();
});