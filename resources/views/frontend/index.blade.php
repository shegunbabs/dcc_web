@extends('layouts.frontend')

@section('content')
    @include('frontend.partials.slider', ['img_url' => '/fe/img/home-bg-1.jpg', 'sub_heading'=>'Latest sermon', 'main_text'=>'Don\'t look back', 'button_text'=>'Check Now'])
    <section>
        <div class="container">
            <div class="section">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="who-content" style="text-align: left;">
                            <h4 class="who-heading">Greetings & Welcome</h4>
                            <p>What makes David’s Christian Centre so special is not just the awesome worship experience or the way the Word is taught
                                with humor, simplicity, vibrancy, and practicality. </p>
                            <p>It’s also our core values, what we like to call the 4R’s of DCC that distinguishes us and makes us so unique and
                                outstanding.</p>
                            <a href="#" class="bg-purple hover:text-white hover:bg-purple-dark text-white py-4 px-8 rounded-full mt-3 inline-block">Where
                                to start</a>

                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="who-img block-effect">
                            <div class="tg-videoarea">
                                <figure>
                                    <img src="/fe/img/we-are-01.jpg" class="img-responsive" alt="image description">
                                    <figcaption><a href="#" class="tg-btnplay" data-rel="prettyPhoto[gallery]">
                                            <img src="/fe/img/btn-playicon-01.jpg" alt="image description"></a></figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <section id="event-counter" class="event-counter-main-block">
        <div class="container-fluid">
            <div class="row row-full-spacing">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="event-counter-blockOne">
                        <div class="bg-img" style="background-image: url('/fe/img/event-bg.jpg')">
                            <div class="overlay-bg"></div>
                            <div class="event-counter-block">
                                <div class="row">
                                    <div class="col-sm-offset-3 col-sm-1 col-xs-offset-2 col-xs-1">
                                        <div class="event-counter-icon"><i class="fa fa-calendar"></i></div>
                                    </div>
                                    <div class="col-sm-8 col-xs-9">
                                        <div class="event-counter-content">
                                            <h4 class="event-counter-heading">Latest Events</h4>
                                            <h5 class="event-counter-subheading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Vestibulum.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="event-counter-blockTwo">
                        <div class="bg-img" style="background-image: url('/fe/img/counter-bg.jpg')">
                            <div class="overlay-bg"></div>
                            <div class="event-counter-block">
                                <div class="event-countdown">
                                    <div class="coming-countdown" data-countdown="2017/9/30"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--<section id="listen-section" class="listen-block">--}}
        {{--<div class="listen-item">--}}
            {{--<div class="listen-element">--}}
                {{--<img class="img-responsive listen-bg" src="/fe/img/video-bg.png" alt="">--}}
                {{--<div class="overlay-bg"></div>--}}
                {{--<div class="listen-body">--}}
                    {{--<a href="javascript:void(0);" class="btn-play">--}}
                        {{--<img src="/fe/img/play-icon.png" alt="btn-play" style="vertical-align:inherit"></a>--}}
                    {{--<p>Listen Now</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

    <section id="section-new">

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12">
                    <figure>
                        <div class="media" style="background-image:url('/fe/img/video-bg.png');"></div>
                        <figcaption>
                            <svg viewBox="0 0 200 200" version="1.1" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg">
                                <defs>
                                    <mask id="mask" x="0" y="0" width="100%" height="100%">
                                        <rect id="alpha" x="0" y="0" width="100%" height="100%"></rect>
                                        <text class="title" dx="50%" dy="2.5em">ENJOY</text>
                                        <text class="title" dx="50%" dy="3.5em">EVERY</text>
                                        <text class="title" dx="50%" dy="4.5em">MOMENT</text>
                                    </mask>
                                </defs>
                                <rect id="base" x="0" y="0" width="100%" height="100%"></rect>
                            </svg>
                            <div class="body">
                                <p>Enamel pin selvage health goth edison bulb, venmo glossier tattooed hella butcher cred iPhone.</p>
                            </div>
                        </figcaption><a href="#"></a>
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section id="sermons" class="sermons-vertical-main-block sermons-vOne">
        <div class="container-fluid">
            <div class="section text-center">
                <h3>Latest Sermons</h3>
            </div>
        </div>
        <div id="sermons-slider" class="sermons-slider">
            <div class="item sermons-block">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                        <div class="sermons-vertical-block sermons-img-contentOne">
                            <div class="sermons-img">
                                <img src="/fe/img/sermons-01.jpg" class="img-responsive" alt="sermons-img-1">
                            </div>
                            <div class="sermons-icon">
                                <ul>
                                    <li>
                                        <a class="popup-video"
                                           href="#"><i
                                                    class="flaticon-video-player"></i></a>
                                    </li>
                                    <li>
                                        <a class="popup-video"
                                           href="#"><i
                                                    class="flaticon-headphones"></i></a>
                                    </li>
                                    <li><a href="#" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                    <li><a href="#" class="pdf"><i class="flaticon-pdf"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="sermons-contentOne">
                            <a href="#"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="#">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis</p>
                            <a class="read-more" href="#">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item sermons-block">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="sermons-contentTwo">
                            <a href="#"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="#">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis</p>
                            <a class="read-more" href="#">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                        <div class="sermons-vertical-block sermons-img-contentTwo">
                            <div class="sermons-img">
                                <img src="/fe/img/sermons-02.jpg" class="img-responsive" alt="sermons-img-2">
                            </div>
                            <div class="sermons-icon">
                                <ul>
                                    <li>
                                        <a class="popup-video"
                                           href="#"><i
                                                    class="flaticon-video-player"></i></a>
                                    </li>
                                    <li>
                                        <a class="popup-video"
                                           href="#"><i
                                                    class="flaticon-headphones"></i></a>
                                    </li>
                                    <li><a href="#" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                    <li><a href="#" class="pdf"><i class="flaticon-pdf"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item sermons-block">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                        <div class="sermons-vertical-block sermons-img-contentOne">
                            <div class="sermons-img">
                                <img src="/fe/img/sermons-01.jpg" class="img-responsive" alt="sermons-img-3">
                            </div>
                            <div class="sermons-icon">
                                <ul>
                                    <li>
                                        <a class="popup-video" href="#"><i
                                                    class="flaticon-video-player"></i></a>
                                    </li>
                                    <li>
                                        <a class="popup-video"
                                           href="#"><i
                                                    class="flaticon-headphones"></i></a>
                                    </li>
                                    <li><a href="#" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                    <li><a href="#" class="pdf"><i class="flaticon-pdf"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="sermons-contentOne">
                            <a href="#"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="#">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis</p>
                            <a class="read-more" href="#">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item sermons-block">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="sermons-contentTwo">
                            <a href="#"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="#">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis</p>
                            <a class="read-more" href="#">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                        <div class="sermons-vertical-block sermons-img-contentTwo">
                            <div class="sermons-img">
                                <img src="/fe/img/sermons-02.jpg" class="img-responsive" alt="sermons-img-4">
                            </div>
                            <div class="sermons-icon">
                                <ul>
                                    <li>
                                        <a class="popup-video" href="#"><i class="flaticon-video-player"></i></a>
                                    </li>
                                    <li>
                                        <a class="popup-video"
                                           href="#"><i
                                                    class="flaticon-headphones"></i></a>
                                    </li>
                                    <li><a href="#" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                    <li><a href="#" class="pdf"><i class="flaticon-pdf"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection