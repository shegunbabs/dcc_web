@extends('layouts.frontend')

@section('content')
    @include('frontend.headers.section-header', ['header'=>'Sermons', 'bg_img_url'=>'/fe/img/top-banner.jpg'])
    <section id="sermons" class="sermons-vertical-main-block sermons-vTwo">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="sermons-vertical-block">
                        <div class="sermons-img block-effect">
                            <img src="/fe/img/sermons-page-01.jpg" class="img-responsive" alt="sermons-img-1">
                        </div>
                        <div class="sermons-icon">
                            <ul>
                                <li>
                                    <a class="popup-video" href="#">
                                        <i class="fa fa-film" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="popup-video" href="#">
                                        <i class="flaticon-headphones"></i>
                                        <i class="fa fa-headphones" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank" class="cloud">
                                        <i class="flaticon-cloud-computing"></i>
                                        <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sermons-content">
                        <div class="sermons-item">
                            <a href="#"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="#">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis, tortor nibh cursus lorem, ac iaculis metus sem vitae purus.</p>
                            <a class="read-more" href="#">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="sermons-vertical-block">
                        <div class="sermons-img block-effect">
                            <img src="/fe/img/sermons-page-02.jpg" class="img-responsive" alt="sermons-img-2">
                        </div>
                        <div class="sermons-icon">
                            <ul>
                                <li>
                                    <a class="popup-video" href="#"><i class="flaticon-video-player"></i></a>
                                </li>
                                <li>
                                    <a class="popup-video" href="#"><i class="flaticon-headphones"></i></a>
                                </li>
                                <li><a href="#" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                <li><a href="#" class="pdf"><i class="flaticon-pdf"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sermons-content">
                        <div class="sermons-item">
                            <a href="#"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="#">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis, tortor nibh cursus lorem, ac iaculis metus sem vitae purus.</p>
                            <a class="read-more" href="#">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-top-spacing">
                    <div class="sermons-vertical-block">
                        <div class="sermons-img block-effect">
                            <img src="/fe/img/sermons-page-03.jpg" class="img-responsive" alt="sermons-img-3">
                        </div>
                        <div class="sermons-icon">
                            <ul>
                                <li>
                                    <a class="popup-video" href="#"><i class="flaticon-video-player"></i></a>
                                </li>
                                <li>
                                    <a class="popup-video" href="#"><i class="flaticon-headphones"></i></a>
                                </li>
                                <li><a href="#" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                <li><a href="#" class="pdf"><i class="flaticon-pdf"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sermons-content">
                        <div class="sermons-item">
                            <a href="#"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="#">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis, tortor nibh cursus lorem, ac iaculis metus sem vitae purus.</p>
                            <a class="read-more" href="#">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-top-spacing">
                    <div class="sermons-vertical-block">
                        <div class="sermons-img block-effect">
                            <img src="/fe/img/sermons-page-04.jpg" class="img-responsive" alt="sermons-img-4">
                        </div>
                        <div class="sermons-icon">
                            <ul>
                                <li>
                                    <a class="popup-video" href="https://www.youtube.com/watch?v=YE7VzlLtp-4"><i class="flaticon-video-player"></i></a>
                                </li>
                                <li>
                                    <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                                </li>
                                <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sermons-content">
                        <div class="sermons-item">
                            <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis, tortor nibh cursus lorem, ac iaculis metus sem vitae purus.</p>
                            <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-top-spacing">
                    <div class="sermons-vertical-block">
                        <div class="sermons-img block-effect">
                            <img src="/fe/img/sermons-page-05.jpg" class="img-responsive" alt="sermons-img-5">
                        </div>
                        <div class="sermons-icon">
                            <ul>
                                <li>
                                    <a class="popup-video" href="https://video-vh.s3.envato.com/h264-video-previews/c865f715-884d-47d7-8f1d-ef312e0c5e99/19716501.mp4"><i class="flaticon-video-player"></i></a>
                                </li>
                                <li>
                                    <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                                </li>
                                <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sermons-content">
                        <div class="sermons-item">
                            <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis, tortor nibh cursus lorem, ac iaculis metus sem vitae purus.</p>
                            <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-top-spacing">
                    <div class="sermons-vertical-block">
                        <div class="sermons-img block-effect">
                            <img src="/fe/img/sermons-page-06.jpg" class="img-responsive" alt="sermons-img-6">
                        </div>
                        <div class="sermons-icon">
                            <ul>
                                <li>
                                    <a class="popup-video" href="https://video-vh.s3.envato.com/h264-video-previews/669f83b8-ced7-47f4-9fd8-a6ea67326539/756957.mp4"><i class="flaticon-video-player"></i></a>
                                </li>
                                <li>
                                    <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                                </li>
                                <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sermons-content">
                        <div class="sermons-item">
                            <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis, tortor nibh cursus lorem, ac iaculis metus sem vitae purus.</p>
                            <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-top-spacing">
                    <div class="sermons-vertical-block">
                        <div class="sermons-img block-effect">
                            <img src="/fe/img/sermons-page-07.jpg" class="img-responsive" alt="sermons-img-7">
                        </div>
                        <div class="sermons-icon">
                            <ul>
                                <li>
                                    <a class="popup-video" href="https://vimeo.com/1084537"><i class="flaticon-video-player"></i></a>
                                </li>
                                <li>
                                    <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                                </li>
                                <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sermons-content">
                        <div class="sermons-item">
                            <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis, tortor nibh cursus lorem, ac iaculis metus sem vitae purus.</p>
                            <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-top-spacing">
                    <div class="sermons-vertical-block">
                        <div class="sermons-img block-effect">
                            <img src="/fe/img/sermons-page-08.jpg" class="img-responsive" alt="sermons-img-8">
                        </div>
                        <div class="sermons-icon">
                            <ul>
                                <li>
                                    <a class="popup-video" href="https://www.youtube.com/watch?v=YE7VzlLtp-4"><i class="flaticon-video-player"></i></a>
                                </li>
                                <li>
                                    <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                                </li>
                                <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                                <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sermons-content">
                        <div class="sermons-item">
                            <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                            <div class="sermons-desc">
                                <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                                <p class="sermons-date">Date : 02 - 01 - 2017</p>
                            </div>
                            <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis, tortor nibh cursus lorem, ac iaculis metus sem vitae purus.</p>
                            <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 load-more text-center">
                    <span class="load-more-btn"><a href="#" class="btn btn-default">Load More</a></span>
                </div>
            </div>
        </div>
    </section>
    <!--  end sermons vtwo -->
    <!--  Trending sermons -->
    <section id="trending-sermons" class="trending-main-block sermons-hOne">
        <div class="container-fluid">
            <div class="section text-center">
                <h3 class="section-heading">Trending Sermons</h3>
                <h5 class="sub-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at  euismod ex, Maeceans sit amet sollicitudin ex.</h5>
            </div>
        </div>
        <div id="trending-sermons-slider" class="trending-sermons-slider">
            <div class="item sermons-block">
                <div class="sermons-img">
                    <img src="/fe/img/sermons-01.jpg" class="img-responsive" alt="sermons-img-1">
                </div>
                <div class="item sermons-horizontal-block">
                    <div class="sermons-content">
                        <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                        <div class="sermons-desc">
                            <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                            <p class="sermons-date">Date : 02 - 01 - 2017</p>
                        </div>
                        <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis</p>
                        <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                    <div class="sermons-icon">
                        <ul>
                            <li>
                                <a class="popup-video" href="https://video-vh.s3.envato.com/h264-video-previews/c865f715-884d-47d7-8f1d-ef312e0c5e99/19716501.mp4"><i class="flaticon-video-player"></i></a>
                            </li>
                            <li>
                                <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                            </li>
                            <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                            <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="item sermons-block">
                <div class="sermons-img">
                    <img src="/fe/img/sermons-02.jpg" class="img-responsive" alt="sermons-img-2">
                </div>
                <div class="item sermons-horizontal-block">
                    <div class="sermons-content">
                        <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                        <div class="sermons-desc">
                            <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                            <p class="sermons-date">Date : 02 - 01 - 2017</p>
                        </div>
                        <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis</p>
                        <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                    <div class="sermons-icon">
                        <ul>
                            <li>
                                <a class="popup-video" href="https://video-vh.s3.envato.com/h264-video-previews/9e51e6a1-6c95-4e54-9572-b6938581babd/19708248.mp4"><i class="flaticon-video-player"></i></a>
                            </li>
                            <li>
                                <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                            </li>
                            <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                            <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="item sermons-block">
                <div class="sermons-img">
                    <img src="/fe/img/sermons-03.jpg" class="img-responsive" alt="sermons-img-3">
                </div>
                <div class="item sermons-horizontal-block">
                    <div class="sermons-content">
                        <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                        <div class="sermons-desc">
                            <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                            <p class="sermons-date">Date : 02 - 01 - 2017</p>
                        </div>
                        <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis</p>
                        <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                    <div class="sermons-icon">
                        <ul>
                            <li>
                                <a class="popup-video" href="https://video-vh.s3.envato.com/h264-video-previews/e0657980-289f-4c5a-9ba0-2c730b9c990a/19398708.mp4"><i class="flaticon-video-player"></i></a>
                            </li>
                            <li>
                                <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                            </li>
                            <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                            <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="item sermons-block">
                <div class="sermons-img">
                    <img src="/fe/img/sermons-04.jpg" class="img-responsive" alt="sermons-img-4">
                </div>
                <div class="item sermons-horizontal-block">
                    <div class="sermons-content">
                        <a href="sermons-details.html"><h4 class="sermons-heading">Praesent elementum ante.</h4></a>
                        <div class="sermons-desc">
                            <p class="sermons-speaker">Speaker : <a href="sermons-details.html">Johan Tom</a></p>
                            <p class="sermons-date">Date : 02 - 01 - 2017</p>
                        </div>
                        <p class="sermons-dtl">Aliquam laoreet orci quis risus vulputate tem Quisque aliquet, ipsum at mattis mattis</p>
                        <a class="read-more" href="sermons-details.html">Read More<i class="fa fa-long-arrow-right"></i></a>
                    </div>
                    <div class="sermons-icon">
                        <ul>
                            <li>
                                <a class="popup-video" href="https://vimeo.com/1084537"><i class="flaticon-video-player"></i></a>
                            </li>
                            <li>
                                <a class="popup-video" href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72365194&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"><i class="flaticon-headphones"></i></a>
                            </li>
                            <li><a href="uploads/demo.zip" target="_blank" class="cloud"><i class="flaticon-cloud-computing"></i></a></li>
                            <li><a href="uploads/demo.pdf" class="pdf"><i class="flaticon-pdf"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection