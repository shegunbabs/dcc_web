@extends('layouts.frontend')

@section('content')
    @include('frontend.headers.section-header', ['header'=>'Our Core Values', 'bg_img_url'=>'/fe/img/top-banner.jpg'])
    <!--  pastor quote -->
    <section class="pastor-quote-main-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="pastor-quote-block">
                        <p>What makes David’s Christian Centre so special is not just the awesome worship experience or the way the Word is taught with humor, simplicity, vibrancy and practicality. It’s also our core values, what we like to call the 4R’s of DCC that distinguish us and makes us so unique and outstanding</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  end pastor quote -->

    <!--  ministry  -->
    <section id="ministry" class="ministry-main-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ministry-block">
                        <div class="ministry-img block-effect">
                            <a href="#"><img src="/fe/img/ministry-01.jpg" class="img-responsive" alt="ministry-img-01"></a>
                        </div>
                        <div class="ministry-content">
                            <a href="#"><h4 class="ministry-heading">Result</h4></a>
                            <p class="ministry-dtl">In DCC, we believe it’s possible to have victory in every area of your life. We have a strong covenant with God with the mandate that “As David never lost a battle so will His walk with us be” hence our name. Because we know the Word of God works absolutely, we just don’t believe in teaching the Word but in experiencing results in every area of our life; spiritually, financially, academically, health, marriage, career. In DCC, we always win!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ministry-block">
                        <div class="ministry-img block-effect">
                            <a href="#"><img src="/fe/img/ministry-02.jpg" class="img-responsive" alt="ministry-img-02"></a>
                        </div>
                        <div class="ministry-content">
                            <a href="#"><h4 class="ministry-heading">Real Life</h4></a>
                            <p class="ministry-dtl">Because you are a Christian doesn’t mean you can’t enjoy your everyday life. Life is real and Jesus came so that we might have life and enjoy it to the fullest. Being a Christian doesn’t have to be boring. It is an enjoyable walk with God while having fun shared experiences with other people.</p>

                            <p>In DCC, we are not just tongue-speaking, Spirit-filled Christians; we are fun-loving, down-to-earth, real people.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-top-spacing">
                    <div class="ministry-block">
                        <div class="ministry-img block-effect">
                            <a href="#"><img src="/fe/img/ministry-03.jpg" class="img-responsive" alt="ministry-img-03"></a>
                        </div>
                        <div class="ministry-content">
                            <a href="#"><h4 class="ministry-heading">Reach Out In Love</h4></a>
                            <p class="ministry-dtl">DCC is a family! We love God and we love people and we believe in reaching out to others with the love of God.</p>

                            <p>We are often involved in a lot of outreaches; community service and our DCC connect groups which help foster fellowship among us.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 row-top-spacing">
                    <div class="ministry-block">
                        <div class="ministry-img block-effect">
                            <a href="#"><img src="/fe/img/ministry-04.jpg" class="img-responsive" alt="ministry-img-04"></a>
                        </div>
                        <div class="ministry-content">
                            <a href="#"><h4 class="ministry-heading">Right Living</h4></a>
                            <p class="ministry-dtl">We don’t only talk the talk, we walk the walk. We believe that the Bible is the manual for us.  We are not just Christians on Sundays; we live by the Word daily and place a premium on it because we know the Word works!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  end ministry -->

    <!--  pastor detail -->
    <section id="pastor-dtl-page" class="pastor-dtl-block">
        <div class="container-fluid">
            <div class="section text-center">
                <h3>Our Pastors</h3>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="pastor-img">
                        <img src="/fe/img/pastor-01.jpg" class="img-responsive" alt="pastor-img-1">
                    </div>
                    <div class="pastor-content">
                        <h3 class="pastor-id">KINGSLEY OKONKWO</h3>
                        <p class="pastor-post">Senior Pastor</p>
                        <hr>
                        <div class="social-icon social-two">
                            <ul>
                                <li><a href="https://plus.google.com/" target="_blank" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="https://www.twitter.com/" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.youtube.com/" target="_blank" class="youtube"><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-12 col-xs-12">
                            <div>
                                <p>If you’ve ever wondered what Jesus would look like in this time and age, then you need not look far. Pastor K as he’s fondly called paints a vivid picture. A ‘crazy’ lover of God with a heart full of compassion for people, he preaches the truth in God’s Word with boldness and without any reservations.</p>

                                <p>With a vision to “raise a victorious people,” he has been on a mission to “school and train people through the word of faith into a life of 100% victory 100% of the time” for 20 years and still counting</p>

                                <p>Pastor Kingsley is not a man who just teaches, he lays emphasis on doing because that’s the only way to get results. He is a man of faith with numerous results to show for it and he constantly and consistently, with joy, teaches Bible-based principles so that people can live victoriously.</p>

                                <p>He plays tennis and football as a form of body exercise, watches movies when he can, rides a power bike because he loves to and travels round the world while still preaching the gospel on his trips</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" style="padding-top: 50px;">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-12 col-xs-12">
                                <div>
                                    <p>She is fondly called ‘Mama’ and the name is rightly placed as she is a mother to all, always seeking the best for her children (members of DCC and even beyond).Called to serve beside Pastor K, her best friend, mentor, Pastor and husband, Pastor M gives a heart that understands, ears that listen and a mind that offers guidance and counsel to all. A deep and true lover of God which is one of the many qualities that attracted Pastor K to her, Pastor M joyfully serves, giving her all to the ministry she’s been called to.</p>

                                    <p>She is also the founder and convener of the JUST US GIRLS CONFERENCE which has attracted women in their thousands. It is a ministry centered on helping women maximize their full potentials and get them to a point where they can stand out in their various fields.</p>

                                    <p>She has an immense love for drama and appreciates dance but what she truly enjoys is writing. This has birthed the popular justusgirlsnaija blog http://justusgirlsnaija.wordpress.com/where she shares her thoughts and feelings on various issues but above all, inspires.</p>

                                    <p>Pastor K and Pastor M are passionate about relationships and marriages. They believe you can have the marriage of your dreams…you only have to work at it. To equip people with the knowledge they need, they run a Love, Dating & Marriage seminar every month and show it on TV every week. Lots of lives and homes have been blessed by this ministry. PK and PM are happily married and are blessed with children</p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="pastor-img">
                            <img src="/fe/img/pastor-02.jpg" class="img-responsive" alt="pastor-img-1">
                        </div>
                        <div class="pastor-content">
                            <h3 class="pastor-id">MILDRED KINGSLEY-OKONKWO</h3>
                            <p class="pastor-post">Pastor</p>
                            <hr>
                            <div class="social-icon social-two">
                                <ul>
                                    <li><a href="https://plus.google.com/" target="_blank" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="https://www.twitter.com/" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.facebook.com/" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://www.youtube.com/" target="_blank" class="youtube"><i class="fa fa-youtube-play"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection