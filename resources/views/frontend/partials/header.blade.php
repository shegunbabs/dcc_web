<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]> -->
<html lang="en">
<!-- <![endif]-->
<head>
    <title>David's Christian Centre</title>
    <meta charset="utf-8"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta name="description" content=""/>
    <meta name="keywords" content="">
    <meta name="author" content=""/>
    <meta name="MobileOptimized" content="320"/>
    <link rel="icon" type="image/icon" href="/fe/img/favicon.ico">
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
    <link href="/fe/css/menumaker.css" rel="stylesheet" type="text/css"/>
    <link href="/fe/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="/fe/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/fe/css/app.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="preloader">
    <div class="status">
        <div class="status-message">
        </div>
    </div>
</div>
<div class="nav-bar navbar-fixed-top border-none">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                <div class="logo">
                    <a href="/"><img src="/fe/img/logo.png" alt="logo"></a>
                </div>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                <div class="navigation-menu">
                    <div id="cssmenu">
                        <ul class="float-right">
                            <li><a href="/">Home</a></li>
                            <li><a href="#">ABOUT</a>
                                <ul>
                                    <li><a href="{{route('about.church')}}">The Church</a></li>
                                    <li><a href="#">Testimonies</a></li>
                                    <li><a href="#">Forms</a></li>
                                </ul>
                            </li>
                            <li><a href="#">MEDIA</a>
                                <ul>
                                    <li><a href="{{route('sermons')}}">Sermons</a></li>
                                    <li><a href="#">Live Broadcast</a></li>
                                </ul>
                            </li>
                            <li><a href="#">MINISTRIES</a>
                                <ul>
                                    <li><a href="http://www.lovedatingandmarriage.org/">LDM</a></li>
                                    <li><a href="#">Preserved generation</a></li>
                                    <li><a href="#">David’s Army</a></li>
                                    <li><a href="#">Youth church</a></li>
                                    <li><a href="#">Just us girls</a></li>
                                </ul>
                            </li>
                            <li><a href="#">GET INVOLVED</a>
                                <ul>
                                    <li><a href="#">Discipleship</a></li>
                                    <li><a href="ministry.html">Workforce</a></li>
                                    <li><a href="#">Fellowships</a></li>
                                    <li><a href="#">Outreaches</a></li>
                                </ul>
                            </li>
                            <li><a href="event.html">EVENTS</a></li>
                            <li><a href="#">BLOG</a>
                                <ul>
                                    <li><a href="blog.html">DCC life & Style Blog</a></li>
                                    <li><a href="https://justusgirlsnaija.com/">Jug Blog</a></li>
                                </ul>
                            </li>
                            <li><a href="#">RESOURCES</a>
                                <ul>
                                    <li><a href="#">Library</a></li>
                                    <li><a href="#">Forms</a></li>
                                    <li><a href="#">E-versions</a></li>
                                    <li><a href="#">Books</a></li>
                                </ul>
                            </li>
                            <li><a href="contact.html">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="#" class="bg-purple hover:bg-purple-dark hover:text-white text-white font-bold py-2 px-8 rounded-full fixed pin-t pin-r z-auto" style="z-index:1031;top:20px;right:20px">Give</a>