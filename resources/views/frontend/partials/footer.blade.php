<footer id="footer" class="footer-main-block">
    <div class="bg-img" style="background-image: url('/fe/img/footer-bg.jpg')">
        <div class="overlay-bg"></div>
        <div class="container">
            <div class="row footer-block">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-logo">
                        <h3>MAINLAND CHURCH</h3>
                    </div>
                    <div class="row footer-content">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Loc:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><p>David’s Christian Centre, Victory Dome, Behind PHCN, Fatgbems Bus stop
                                Mile 2, Lagos State</p></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Phone:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><p class="contact-no"><a href="tel:012123456789">012 123 456 789</a><br><a
                                        href="tel:012123456789">012 123 456 789</a></p></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Email:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <p class="mail-to"><a href="mailto:wporganic@example.com?Subject=Hello%20again"
                                                  target="_top">wporganic@example.com</a><br>
                                <a href="mailto:wporganic@example.com?Subject=Hello%20again" target="_top">wporganic@example.com</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-logo">
                        <h3>ISLAND CHURCH</h3>
                    </div>
                    <div class="row footer-content">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Loc:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><p>David’s Christian Centre, Beside Meadow Hall School, Elegushi Beach Road,
                                3rd Roundabout, Lagos </p></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Phone:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><p class="contact-no"><a href="tel:012123456789">012 123 456 789</a><br><a
                                        href="tel:012123456789">012 123 456 789</a></p></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Email:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <p class="mail-to"><a href="mailto:wporganic@example.com?Subject=Hello%20again"
                                                  target="_top">wporganic@example.com</a><br>
                                <a href="mailto:wporganic@example.com?Subject=Hello%20again" target="_top">wporganic@example.com</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-logo">
                        <h3>SERVICE TIMES</h3>
                    </div>
                    <div class="row footer-content">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Sundays:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9"><p>7:30am – <b>Mainland Centre</b> <br> 9:00am – <b>Mainland Centre</b> <br>10:00am
                                – <b>Island Centre</b></p></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Tuesdays:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">6.30pm – <b>Island Centre</b></p></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><p>Wednesdays:</p></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">6.30pm – <b>Mainland Centre</b></p></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer-dtl text-center">
                    <p>Copyright &copy; 2018 <a href="/">DCC</a> | David's Christian Centre</p>
                </div>
            </div>
        </div>
    </div>
</footer>