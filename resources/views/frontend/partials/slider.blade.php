<header class="slider-main-block">
    <div class="item home-slider-img" style="background-image: url('{{!empty($img_url) ? $img_url : '/fe/img/home-bg-1.jpg'}}')">
        <div class="overlay-bg"></div>
        <div class="container">
            <div class="slider-dtl" style="text-align: left;">
                <h6 class="slider-heading">{{!empty($sub_heading) ? $sub_heading : 'latest sermon'}}</h6>
                <h1 class="slider-heading">{{$main_text}}</h1>
                <div class="slider-btn">
                    <span class="slider-donate-btn">
                        <a style="margin-left: unset;" href="#"
                           class="bg-purple text-white hover:text-white hover:bg-purple-dark font-bold py-5 px-10 rounded-full mt-1 inline-block">
                            {{!empty($button_text) ? $button_text : 'watch now'}}
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</header>