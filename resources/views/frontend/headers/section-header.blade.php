<header id="page-banner" class="banner-main-block">
    <div class="banner-img" style="background-image: url('{{!empty($bg_img_url) ? $bg_img_url : '/fe/img/top-banner.jpg'}}')">
        <div class="overlay-bg"></div>
        <div class="container">
            <div class="banner-block">
                <h3 class="section-heading">{{$header}}</h3>
            </div>
        </div>
    </div>
</header>