@extends('layouts.backend')
@section('page-title', 'Create New Event')
{{--@section('page-subtitle', 'Create Events')--}}

@push('stylesheets')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
@auth
window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + '{{ auth()->user()->api_token }}';
@endauth
    const $vue = new Vue({
        el: '#app',
        data: {
            eventType:{
                name: '',
                description: '',
                visibility: '',
            },
            eventTypes: {},
            start: null,
            event: {
                name: '',
                theme: '',
                event_type_id: '',
                venue: '',
                reg_status: '',
                start_date: '',
                end_date: '',
            },
            errors: {},
        },
        methods:{
            saveEvent($event){
                this.buttonState($event, 'start');
                axios.post('/api/v1/events', this.event)
                    .then(res => {
                        this.buttonState($event, 'finish');
                        this._alert("Event created.", "success");
                        this.clearEventData();
                    })
                    .catch(err => {
                        this.buttonState($event, 'finish');
                        if ( err.response.status === 401 ){
                            this._alert("You are not authenticated.", "warning", false);
                            setTimeout(() => {window.location = "/backend/login"}, 5000);
                            return;
                        }
                        this.errors = err.response.data.errors;
                    });
            },
            saveAddNewEvent(){
                axios.post('/api/v1/event-type/new', {
                    name: this.eventType.name,
                    description: this.eventType.description,
                    visibility: this.eventType.visibility,
                }).then(res => {
                    this.closeModal('addNewEventModal');
                    this.clearEventType();
                    this.allEventType();
                })
                    .catch(err => {
                        this.closeModal('addNewEventModal');
                        console.log(err.response.data)
                    });
            },
            allEventType(){
                axios.get('/api/v1/event-type/all')
                    .then(res => {
                        this.eventTypes = res.data;
                    })
                    .catch(err => {
                        console.log(err.response.data)
                    });
            },
            clearEventType(){
                this.eventType.name = '';
                this.eventType.description = '';
                this.eventType.visibility = '';
            },
            clearEventData(){
                this.event = {};
                this.$refs.startDate.clear();
                this.$refs.endDate.clear();
            },
            addNewType(){
                this.showModal('addNewEventModal');
            },
            showModal($id){
                $(`#${$id}`).modal('show');
            },
            closeModal($id){
                $(`#${$id}`).modal('hide');
            },
            showDate(e){
                console.log(e.target.id);
                flatpickr(`#${e.target.id}`, {
                    minDate: "today",
                    enableTime: true
                });
            },
            buttonState(event, state="start"){
                if (state === "start"){
                    event.target.setAttribute("disabled", true);
                    event.target.innerHTML = "Loading...";
                } else {
                    event.target.removeAttribute("disabled");
                    event.target.innerHTML = "Save Event";
                }
            },
            _alert($message, $type='info', $timeout=4500){
                return new Noty({text: $message, type: $type, timeout: $timeout}).show();
            },
        },
        created(){
            this.allEventType();
        }
    })
</script>
@endpush
@section('content')
    <div class="row" id="app">
        <div class="col-md-7">
            @include('includes.addNewEventModal')
            <div class="card card-default widget">
                <div class="card-heading">
                    <div class="card-controls">
                        <a href="#" class="widget-minify"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <h3 class="card-title">Create New Event</h3>
                </div>
                <div class="card-body">

                    <form method="post">
                        <div class="form-row">
                            <div class="col-md-12 mb-4">
                                <label for="name">Event Name/Title:</label>
                                <input type="text" class="form-control" id="name" placeholder="Event Name/Title"
                                       v-model="event.name" v-bind:class="{'is-invalid' : errors.name}">
                                <div class="invalid-feedback" v-if="errors.name">
                                    @{{ errors.name[0] }}
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-4">
                                <label for="validationServer01">Event Theme</label>
                                <input type="text" class="form-control" id="validationServer01" placeholder="Theme"
                                       v-model="event.theme" v-bind:class="{'is-invalid' : errors.theme}">
                                <div class="invalid-feedback" v-if="errors.theme">
                                    @{{ errors.theme[0] }}
                                </div>
                            </div>
                            <div class="col-md-5 offset-md-1 mb-3">
                                <label for="validationServer01">Event Type</label>
                                <a href="#" @click.prevent="addNewType">
                                    <span class="badge badge-primary btn-xs span-button"><i class="fa-fw ion-plus-circled"></i> Add New Type</span>
                                </a>
                                <select class="custom-select" v-model="event.event_type_id" v-bind:class="{'is-invalid' : errors.event_type_id}">
                                    <option value="">Select One</option>
                                    <option v-for="ev in eventTypes" :value="ev.id">@{{ ev.name }}</option>
                                </select>
                                <div class="invalid-feedback" v-if="errors.event_type_id">
                                    @{{ errors.event_type_id[0] }}
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-4">
                                <label for="venue">Event Venue</label>
                                <input type="text" class="form-control" id="venue" placeholder="Venue"
                                       v-model="event.venue" v-bind:class="{'is-invalid' : errors.venue}">
                                <div class="invalid-feedback" v-if="errors.venue">
                                    @{{ errors.venue[0] }}
                                </div>
                            </div>
                            <div class="col-md-5 offset-md-1 mb-3">
                                <label for="status">Event Status</label>
                                <select name="status" id="status" class="custom-select" v-model="event.reg_status" v-bind:class="{'is-invalid' : errors.reg_status}">
                                    <option value="">Select One</option>
                                    <option value="0">Closed for Registration</option>
                                    <option value="1">Open for Registration</option>
                                </select>
                                <div class="invalid-feedback" v-if="errors.reg_status">
                                    @{{ errors.reg_status[0] }}
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-4s">
                                <label for="start">Event Start Date</label>
                                <date-picker v-model="event.start_date" v-bind:class="{'is-invalid' : errors.start_date}" ref="startDate"></date-picker>
                                <div class="invalid-feedback" v-if="errors.start_date">
                                    @{{ errors.start_date[0] }}
                                </div>
                            </div>
                            <div class="col-md-5 offset-md-1">
                                <label for="start">Event End Date</label>
                                <date-picker v-model="event.end_date" v-bind:class="{'is-invalid' : errors.end_date}" ref="endDate"></date-picker>
                                <div class="invalid-feedback" v-if="errors.end_date">
                                    @{{ errors.end_date[0] }}
                                </div>
                            </div>
                        </div>
                        <div class="form-row mt-4 justify-content-end">
                            <div class="col-md-2">
                                <button type="button" class="btn btn-primary" @click.prevent="saveEvent">Save Event</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div><!-- /.col-md-9 -->
        <div class="col-md-5">
            <div class="panel card-default widget">
                <div class="card-heading">
                    <div class="card-controls">
                        <a href="#" class="widget-minify"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <h3 class="card-title">Event Types</h3>
                </div>
                <div class="card-body">
                    <table class="table" v-if="_.size(eventTypes)">
                        <thead>
                        <tr>
                            <th>Event Type</th>
                            <th class="text-right">Visibility</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="event in eventTypes">
                            <td>@{{ event.name }}</td>
                            <td class="text-right">
                                <div class="chart-legend">
                                    <div class="color background-success" v-if="event.visibility == 1"></div>
                                    <div class="color background-danger" v-else></div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="alert alert-info alert-important" v-else>No Event Types yet</div>
                    <hr>
                    <div class="chart-legend pull-right">
                        <div class="item">
                            <div class="color background-success"></div>
                            Open
                        </div>
                        <div class="item">
                            <div class="color background-danger"></div>
                            Close
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.col-md-3 -->
    </div>
@endsection