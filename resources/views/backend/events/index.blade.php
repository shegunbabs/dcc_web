@extends('layouts.backend')
@section('page-title', 'DCC Events')
@section('page-subtitle', 'Manage Events')
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    @auth
        window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + '{{ auth()->user()->api_token }}';
            @endauth
    const $vue = new Vue({
            el: '#app',
            data: {
                records: {}
            },
            methods: {
                getEvents(){
                    axios.get('/api/v1/events')
                        .then(res => {
                            this.records = res.data;
                        })
                        .catch(err => console.log(err.response.data));
                },
                getEventLink($slug, $reg_status){
                    if ($reg_status === 1)
                    {
                        let content = `${window.location.origin}/event/${$slug}/register`;
                        this.show(content);
                    } else {
                        this.show("This Event is not Open for Registrations")
                    }
                },
                delEvent($id){
                    if (confirm("Are you sure about this???"))
                    {
                        axios(`/api/v1/event/${$id}/del`)
                            .then(res => {
                                this.getEvents();
                                this._alert("Event Delete", 'success');
                            })
                            .catch(err => this._alert("Error deleting event", 'error'));
                    }
                },
                parentFetchPage(page_num){
                    //call url to fetch page
                    axios.get(`/api/v1/events?page=${page_num}`)
                        .then(res => {
                            this.records = res.data;
                        })
                        .catch(err => {
                            console.log(err.response.data);
                        })
                },
                parentFetchUrl(url){
                    axios.get(url)
                        .then(res => {
                            this.records = res.data;
                        })
                },
                _alert($message, $type='info', $timeout=4500){
                    return new Noty({text: $message, type: $type, timeout: $timeout}).show();
                },
                show($content){
                    this.$refs.modal.showModal($content);
                }
            },
            created(){
                this.getEvents();
            }
        });

</script>
@endpush
@section('content')
    <div class="row" id="app">
        <div class="col-md-9 col-sm-8">
            <div class="card card-default widget">
                <div class="card-heading">
                    <div class="card-controls">
                        {{--<select class="form-control form-control-sm d-none d-md-inline-block" style="width:130px; display:inline-block"--}}
                                {{--placeholder="Month">--}}
                            {{--<option>January</option>--}}
                            {{--<option>February</option>--}}
                            {{--<option>March</option>--}}
                            {{--<option>April</option>--}}
                            {{--<option selected>May</option>--}}
                            {{--<option>June</option>--}}
                            {{--<option>July</option>--}}
                            {{--<option>August</option>--}}
                            {{--<option>September</option>--}}
                            {{--<option>October</option>--}}
                            {{--<option>November</option>--}}
                            {{--<option>December</option>--}}
                        {{--</select>--}}
                        <a href="#" class="widget-minify"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <h3 class="card-title">Events Listing</h3>
                </div>
                <div class="card-body">
                    <div v-if="_.size(records.data)">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Event Name</th>
                                <th style="width: 30%">Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="event in records.data">
                                <td>@{{event.name}}</td>
                                <td>
                                    <a href="#" class="link-icon"><i class="fa fa-television" aria-hidden="true" title="View"></i></a>
                                    <a href="#" class="link-icon"><i class="fa fa-reorder" aria-hidden="true" title="Edit"></i></a>
                                    <a href="#" class="link-icon"><i class="fa fa-sign-out" aria-hidden="true" title="Hide"></i></a>
                                    <a href="#" class="link-icon" @click.prevent="getEventLink(event.name_slug, event.reg_status)"><i class="fa fa-wifi" aria-hidden="true" title="Get Registration Link"></i></a>
                                    <a href="#" class="link-icon" @click.prevent="delEvent(event.id)"><i class="fa fa-minus-circle" style="color:red" aria-hidden="true" title="Delete"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <app-paginator v-if="records.next_page_url"
                                       :prev="records.prev_page_url"
                                       :next="records.next_page_url"
                                       :total_pages="records.last_page"
                                       :current_page="records.current_page"
                        @fetchpage="parentFetchPage"
                        @fetchprevpage="parentFetchUrl"
                        @fetchnextpage="parentFetchUrl"></app-paginator>
                    </div>
                    <div class="alert alert-info alert-important" v-else>There is no event data yet</div>
                </div>
            </div>
            <modal-dialog ref="modal" />
        </div><!-- /.col-md-9 -->
        <div class="col-md-3 col-sm-4">
            <div class="panel card-default widget">
                <div class="card-heading">
                    <div class="card-controls">
                        <a href="#" class="widget-minify"><i class="fa fa-chevron-up"></i></a>
                    </div>
                    <h3 class="card-title">Data Window</h3>
                </div>
                <div class="card-body">

                </div>
            </div>
        </div><!-- /.col-md-3 -->
    </div>

@endsection