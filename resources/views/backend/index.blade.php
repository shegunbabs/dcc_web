@extends('layouts.backend')
@section('page-title', 'Dashboard')
@section('page-subtitle', 'Statistics')
@push('scripts')
<script>
    new Vue({
        el: '#app'
    });
</script>
@endpush
@section('content')
    <div class="row" id="app">
        <widget-card title="pages" :value="15" widget-class="col-md-3" icon="ion-document-text"></widget-card>
        <widget-card title="posts" :value="25" widget-class="col-md-3"></widget-card>
        <widget-card title="audios" :value="40" widget-class="col-md-3" icon="ion-speakerphone"></widget-card>
        <widget-card title="audios" :value="40" widget-class="col-md-3" icon="ion-speakerphone"></widget-card>
    </div>
@endsection
