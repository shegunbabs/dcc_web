@extends('backend.partials.auth')
@section('page-title', 'Please Login to Continue')
@section('content')
    <div class="row">
        <div class="col-md-4 mx-auto">
            <div class="page-brand">
                <img src="/img/dcc-web-it.png" alt="DCC-Web-It Logo" style="width: 280px;">
            </div>
            @include('flash::message')

            <div class="card card-default widget">
                <div class="card-heading">
                    <h3 class="card-title">Sign Up</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('register')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   value="{{old('email')}}" placeholder="Email" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username"
                                   class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
                                   value="{{old('username')}}" placeholder="Username" required>
                            @if ($errors->has('username'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input id="password" type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password" required placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Repeat password</label>
                            <input type="password" name="password_confirmation" class="form-control"
                                   placeholder="Repeat Password" required>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input type="checkbox" name="tos" id="tos" value="1" class="form-check-input{{$errors->has('tos')?' is-invalid':''}}">
                                <label for="tos" class="form-check-label">I accept the <a href="#">Terms and Conditions</a></label>
                                @if ($errors->has('tos'))
                                    <span class="invalid-feedback">
                                    <strong>Terms & Conditions must be accepted</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-block btn-lg">Sign Up</button>
                    </form>
                </div>
            </div>

            <div class="text-center">
                <span class="text-muted">Already have an account?</span> <a href="{{route('login')}}">Sign In</a>
            </div>
        </div>
    </div>
@endsection