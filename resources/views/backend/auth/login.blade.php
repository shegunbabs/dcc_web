@extends('backend.partials.auth')
@section('page-title', 'Please Login to Continue')
@section('content')
    <div class="row" id="app">
        <div class="col-md-4 mx-auto">

            <div class="page-brand">
                <img src="/img/dcc-web-it.png" alt="DCC-Web-It Logo" style="width: 280px;">
            </div>

            <div class="card card-default widget">
                <div class="card-heading">
                    <h3 class="card-title">Sign In</h3>
                </div>
                <div class="card-body">
                    <div v-if="errors.message" class="alert alert-danger">@{{errors.message}}</div>
                    <form @keydown="clear($event.target.name)">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email" ref="email" v-model="email" v-bind:class="{'is-invalid' : errors.email}">
                            <div class="invalid-feedback" v-if="errors.email">@{{ errors.email[0] }}</div>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password" ref="password" v-model="password" v-bind:class="{'is-invalid' : errors.password}">
                            <div class="invalid-feedback" v-if="errors.password">@{{ errors.password[0] }}</div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" v-model="rememberMe">
                                    Remember me
                                </label>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success btn-block btn-lg" ref="button" @click.prevent="LogMeIn">Sign
                            In
                        </button>
                    </form>

                    <div class="margin-tb-10">
                        <a href="{{route('password.request')}}">Forgot your password?</a>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <span class="text-muted">Dont have an account?</span> <a href="{{route('register')}}">Sign Up</a>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    let $vm = new Vue({
        el: '#app',
        data: {
            email: '',
            password: '',
            rememberMe: null,
            errors: {},
        },
        methods: {
            LogMeIn(e){
                e.preventDefault();
                let $this = this;
                this.startLoading();
                axios.post('/backend/login', {
                    email: this.email,
                    password: this.password,
                    remember: this.rememberMe,
                }).then((data) => {
                    Swal("Login Success", "You have been logged in successfully", "success")
                        .then(() => { location.assign(data.data.intended); });

                    $this.stopLoading();
                }).catch(err => {
                    Swal("Login Error", "Invalid username/password", "error")
                    $this.errors = err.response.data.errors;
                    $this.stopLoading();
                });
            },
            clear(field){
                if (field) {
                    delete this.errors[field];
                    return;
                } else {
                    this.errors = {};
                }
            },
            startLoading(){
                this.$refs.button.innerHTML = 'Signing in...';
                this.$refs.button.setAttribute('disabled', 'disabled');
                this.$refs.email.setAttribute('disabled', 'disabled');
                this.$refs.password.setAttribute('disabled', 'disabled');
            },
            stopLoading(){
                this.$refs.button.innerHTML = 'Sign In';
                this.$refs.button.removeAttribute('disabled');
                this.$refs.email.removeAttribute('disabled');
                this.$refs.password.removeAttribute('disabled');
            }
        },
        created(){
        }
    });
</script>
@endpush