@extends('spark.partials.auth')

@section('content')
    <div class="row">
        <div class="col-md-4 mx-auto">

            <div class="page-brand">
                <i class="ion ion-ios-pulse-strong" aria-hidden="true"></i>
            </div>

            <div class="card card-default widget">
                <div class="card-heading">
                    <h3 class="card-title">Reset Password</h3>
                </div>
                <div class="card-body">

                    <p class="text-muted text-center">
                        Enter your email address and we'll send you an email with instructions to reset your password.
                    </p>

                    <form>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                        <button type="submit" class="btn btn-success btn-block btn-lg">Reset Password</button>
                    </form>
                </div>
            </div>

            <div class="text-center">
                <span class="text-muted">Already have your credentials? </span> <a href="{{route('login')}}">Sign In</a>
            </div>
        </div>
    </div>
@endsection