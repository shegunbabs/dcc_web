<div class="page-sidebar toggled sidebar">
    <nav class="sidebar-collapse d-none d-md-block">
        <i class="ion ion-ios-arrow-forward show-on-collapsed"></i>
        <i class="ion ion-ios-arrow-back hide-on-collapsed"></i>
    </nav>

    <ul class="nav nav-pills nav-stacked" id="sidebar-stacked">
        <li class="d-md-none">
            <a href="#" class="sidebar-close"><i class="ion ion-ios-arrow-left"></i></a>
        </li>
        <li class="nav-item{{markActiveLink(route('backend.dashboard'))}}">
            <a class="nav-link" href="{{route('backend.dashboard')}}"><i class="ion ion-ios-home"></i> <span class="nav-text">Dashboard</span></a>
        </li>
        <li class="nav-item{{markActiveParentLink(route('backend.events'))}}">
            <a class="nav-container" data-toggle="collapse" data-parent="#sidebar-stacked" href="#e">
                <i class="ion ion-ios-flask"></i> <span class="nav-text">Events <span class="badge badge-pill badge-nav badge-warning">6</span></span>
            </a>
            <ul class="nav nav-pills nav-stacked collapse" id="e">
                <li class="nav-item{{markActiveLink(route('backend.events.create'))}}"><a class="nav-link" href="{{route('backend.events.create')}}">Create Event</a></li>
                <li class="nav-item{{markActiveLink(route('backend.events'))}}"><a class="nav-link" href="{{route('backend.events')}}">LIst Events</a></li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-container dropdown-toggle" data-toggle="collapse" data-parent="#sidebar-stacked" href="#pages">
                <i class="ion ion-document"></i> <span class="nav-text">Pages</span>
            </a>
            <ul class="nav nav-pills nav-stacked collapse" id="pages">
                <li class="nav-item"><a class="nav-link" href="#">All Pages</a></li>
                <li class="nav-item"><a class="nav-link" href="#">New Page</a></li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-container dropdown-toggle" data-toggle="collapse" data-parent="#sidebar-stacked" href="#posts">
                <i class="ion ion-speakerphone"></i> <span class="nav-text">Posts</span>
            </a>
            <ul class="nav nav-pills nav-stacked collapse" id="posts">
                <li class="nav-item"><a class="nav-link" href="#">All Posts</a></li>
                <li class="nav-item"><a class="nav-link" href="#">New Post</a></li>
            </ul>
        </li>

        {{--<li class="nav-item">--}}
            {{--<a class="nav-container" data-toggle="collapse" data-parent="#sidebar-stacked" href="#p3">--}}
                {{--<i class="ion ion-ios-copy"></i> <span class="nav-text">Custom Pages <span class="badge badge-pill badge-nav badge-warning">4</span></span>--}}
            {{--</a>--}}
            {{--<ul class="nav nav-pills nav-stacked collapse" id="p3">--}}
                {{--<li class="nav-item"><a class="nav-link" href="page-login.html">Login</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="page-register.html">Register</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="page-reset-password.html">Reset Password</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="page-lock-screen.html">Lock Screen</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="page-blank.html">Blank Page</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="page-error-404.html">404 Page</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="page-error-500.html">500 Page</a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-container dropdown-toggle" data-toggle="collapse" data-parent="#sidebar-stacked" href="#p4">--}}
                {{--<i class="ion ion-ios-paper"></i> <span class="nav-text">Forms</span>--}}
            {{--</a>--}}
            {{--<ul class="nav nav-pills nav-stacked collapse" id="p4">--}}
                {{--<li class="nav-item"><a class="nav-link" href="form-elements.html">Form Elements</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="form-validation.html">Form Validation</a></li>--}}

                {{--<li class="nav-item"><a class="nav-link" href="form-summernote.html">Summernote</a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-container dropdown-toggle" data-toggle="collapse" data-parent="#sidebar-stacked" href="#p5">--}}
                {{--<i class="ion ion-ios-list"></i> <span class="nav-text">Tables</span>--}}
            {{--</a>--}}
            {{--<ul class="nav nav-pills nav-stacked collapse" id="p5">--}}
                {{--<li class="nav-item"><a class="nav-link" href="table-basic.html">Basic Table</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="table-data.html">Data Table</a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-container" href="calendar.html">--}}
                {{--<i class="ion ion-ios-calendar"></i> <span class="nav-text">Calendar</span>--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-container dropdown-toggle" data-toggle="collapse" data-parent="#sidebar-stacked" href="#p7">--}}
                {{--<i class="ion ion-ios-location"></i> <span class="nav-text">Maps</span>--}}
            {{--</a>--}}
            {{--<ul class="nav nav-pills nav-stacked collapse" id="p7">--}}
                {{--<li class="nav-item"><a class="nav-link" href="map-google.html">Google Maps</a></li>--}}
                {{--<li class="nav-item"><a class="nav-link" href="map-vector.html">Vector Maps</a></li>--}}
            {{--</ul>--}}
        {{--</li>--}}
    </ul>
</div>