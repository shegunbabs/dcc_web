@include('frontend.partials.header')
@yield('content')
@include('frontend.partials.footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/fe/js/owl.carousel.js"></script>
<script type="text/javascript" src="/fe/js/menumaker.js"></script>
<script type="text/javascript" src="/fe/js/jquery.countdown.js"></script>
<script type="text/javascript" src="/fe/js/theme.js"></script>
</body>
</html>
