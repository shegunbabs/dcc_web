<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('page-title', 'DCC Web Admin')</title>

    <!-- External fonts -->
    <link href="https://brick.a.ssl.fastly.net/Montserrat:300,400,500,600,700" rel="stylesheet">

    <!-- NPM Packages -->
    <link href="/css/spark.css" rel="stylesheet">

    @stack('stylesheets')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body> <!-- right-to-left -->

<div class="splash active" id="loader">
    <div class="icon"></div>
</div>

<div class="wrapper">

    <!-- Navbar -->
    @include('backend.partials.navbar')

    <div class="content">
        <header class="page-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-8 page-title-wrapper">
                        <h1 class="page-title">@yield('page-title')</h1>
                        <h2 class="page-subtitle">@yield('page-subtitle')</h2>
                    </div>
                    {{--<div class="col-sm-4 d-none d-md-inline-block page-search-wrapper">--}}
                    {{--<input type="text" class="form-control form-control-lg keyword-search" placeholder="Search for files & reports..">--}}
                    {{--</div>--}}
                </div>
            </div>
        </header>
        <div class="page-body">
            <div class="container-fluid">
                <!-- Sidebar -->
                @include('backend.partials.sidebar')

                <div class="page-content">
                    <div id="page-inner-content">
                        @yield('content')
                    </div>
                    <footer class="site-footer text-center">
                        <p>&copy; 2018 Spark</p>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="#">Home</a></li>
                            <li class="list-inline-item"><a href="#">Support</a></li>
                            <li class="list-inline-item"><a href="#">FAQ</a></li>
                            <li class="list-inline-item"><a href="#">Contact</a></li>
                        </ul>
                    </footer>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script>
    let windowHeight = window.innerHeight;
    windowHeight = windowHeight - (110 + 174) - 30;
    document.getElementById("page-inner-content").style.minHeight = windowHeight + 'px';
</script>
@stack('scripts')
</body>
</html>
