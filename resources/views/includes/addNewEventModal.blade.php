<div class="modal fade" id="addNewEventModal" tabindex="-1" role="dialog" aria-labelledby="addNewEventModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addNewEventModalTitle">Add New Event Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-row justify-content-center">
                        <div class="col-md-8 mb-4">
                            <label for="name">Event Type Name</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Name" v-model="eventType.name">
                        </div>
                        <div class="col-md-8 mb-4">
                            <label for="name">Event Description</label>
                            <textarea name="description" id="description" rows="2" placeholder="Description" v-model="eventType.description"
                                      class="form-control"></textarea>
                        </div>
                        <div class="col-md-8 mb-3">
                            <label for="visibility">Visible?</label>
                            <div class="radio">
                                <label class="mr-5">
                                    <input type="radio" name="visibility" id="visibility" value="1" v-model="eventType.visibility" checked>
                                    Yes
                                </label>
                                <label>
                                    <input type="radio" name="visibility" id="visibility" value="0" v-model="eventType.visibility">
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" @click.prevent="saveAddNewEvent">Save</button>
            </div>
        </div>
    </div>
</div>