#envoy run deploy --message="" --migrate=true --minify=true
@servers(['local' => 'vagrant@192.168.11.10', 'web' => 'tech@dcclagos.tk'])

@task('push', ['on' => 'local'])
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Start push command.....==========\n"

    #navigate to project directory
    cd /home/vagrant/projects/dcc_web

    #add untracked files
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Add untracked files....=========="
    git add .

    #add a commit message if present
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Start git commit.......=========="

    @if ($message)
        git commit -m "{{ $message }}"
    @else
        git commit -m "regular updates"
    @endif

    #push to the master Head
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Git push to master......=========="
    git push -u origin master

    #print a quote
    php artisan inspire

    #insert an extra line
    printf "\n"
@endtask

@task('pull-on-server', ['on' => 'web'])
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Pull on server started==========\n"

    #navigate to project folder
    cd /var/www/dccweb
    # sudo git pull --rebase=false
    # sudo git stash

    #clear cached routes & configs
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Clear cache & routes=========="
    php artisan route:clear
    php artisan config:clear

    #do git pull
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Git pull start ......=========="
    sudo git pull

    #do composer update
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Composer update start.=========="
    sudo composer update

    #cache routes and configs
    printf "$(date '+%d-%m-%Y %H-%M-%S')==========Cache route & configs.=========="
    sudo php artisan route:cache
    sudo php artisan config:cache

    #migrate pending tables
    @if ($migrate)
        printf "$(date '+%d-%m-%Y %H-%M-%S')==========Migrate pending tables=========="
        php artisan migrate
    @endif
@endtask

@story('deploy')
    push
    pull-on-server
@endstory