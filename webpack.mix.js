let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


mix.styles([
    'public/css/app.css',
    'public/assets/lib/css/spark.css',
    'public/assets/lib/font-awesome/css/font-awesome.min.css',
    'public/assets/lib/ionicons/css/ionicons.min.css',
    // 'public/assets/lib/spark.css'
], 'public/css/spark.css');

mix.browserSync('dcc-web.acc');