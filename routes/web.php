<?php

Route::get('test', 'TestController@index');
Route::get('monitor', 'TestController@monitor');



Route::get('/',             'FrontEnd\HomeController@index');
Route::get('the-church',    'FrontEnd\HomeController@theChurch')->name('about.church');
Route::get('sermons',       'FrontEnd\HomeController@sermons')->name('sermons');
Route::get('workforce',     'FrontEnd\HomeController@workforce')->name('workforce');




Route::prefix('backend')->group(function () {
    Auth::routes();
    Route::get('logout', 'Auth\LoginController@logout')->name('backend.logout');
    Route::get('/',     'HomeController@backend')->name('backend.dashboard')->middleware('auth');
    Route::get('events/list',        'EventController@index')->name('backend.events');
    Route::get('events/create', 'EventController@create')->name('backend.events.create');
});






Route::get('event/{slug}/register', 'EventRegController@register');
Route::get('/home', 'HomeController@index')->name('home');
