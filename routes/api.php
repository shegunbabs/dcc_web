<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->middleware('auth:api')->group(function(){
    Route::post('event-type/new',   'API\ApiController@newEventType');
    Route::get('event-type/all',    'API\ApiController@allEventType');

    Route::post('events',           'API\ApiController@postEvent');
    Route::get('events',            'API\ApiController@allEvents');
    Route::get('event/{id}/del',    'API\ApiController@delEvent');
});

Route::prefix('server')->group(function(){
    Route::get('/{url}/ping', function($url){
        $process = new \Symfony\Component\Process\Process('ping -c 4 '.$url);
        if (!$process->isSuccessful()){
            return response()->json(['error'], 422);
        }
        echo response()->json(['success']);
    });
});
